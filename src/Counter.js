import React, { Component } from 'react'

export default class Counter extends Component {
    constructor(){
        super();
        this.state = {
            count : 0
        }
        this.handleIncrement = this.handleIncrement.bind(this)
    }
    handleIncrement(){
        this.setState({
            count: this.state.count + 1
        })
    }
    handleDecrement = ()=>{
        this.setState({
            count: this.state.count - 1
        })
    }
    handleReset = ()=>{
        this.setState({
            count: 0
        })
    }
  render() {
    return (
      <div>
        <h1>Count: {this.state.count}</h1>
        <button onClick={this.handleIncrement}>Increment</button>

        <button onClick={this.handleDecrement}>Decrement</button>
        
        <button onClick={this.handleReset}>Reset</button>
        <div></div>

        <button onClick={()=>{this.setState({count:this.state.count + 5})}}>Inc By 5</button>
        <button onClick={()=>{this.setState({count:this.state.count - 5})}}>Dec By 5</button>
        <button onClick={()=>{this.setState({count:0})}}>Reset</button>


       
      </div>
    )
  }
}
