import React, { Component } from 'react'

export default class LifeCycle extends Component {
    constructor(){
        super();
        console.log("1. Constructor method invoked");
        this.state = {
            count:0
        }
    }
    handleChange = ()=>{
        this.setState({
            count : this.state.count +1
        })
    }
    componentDidMount(){
        console.log("3. Mounted ");
        
    }
    componentDidUpdate(){
        console.log("4. Updated ");
    }
  render() {
    console.log("2. Render method called");
    return (
      <div>
        <h1>Count : {this.state.count}</h1>
        <button onClick={this.handleChange}>Add</button>
      </div>
    )
  }
}
