import React, { useEffect, useState } from 'react'

export default function Products() {
    const [Product, setProduct] = useState([]);
    // console.log(Product);

    // fetch('https://jsonplaceholder.typicode.com/users').then(res=>res.json()).then(data=>console.log(data))
    useEffect(()=>{
        console.log("Mounted");
        fetch('https://jsonplaceholder.typicode.com/users')
        .then(res=>res.json())
        .then(data=>setProduct(data))
        console.log(Product);
    },[])
  return (
    <div>
        <ul>
            {Product.map(item=>(<li key={item.id}>{item.name}</li>))}
        </ul>
    </div>
  )
}
