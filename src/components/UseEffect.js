import React, { useEffect, useState } from 'react'


const UseEffect = () => {
    const [products, setProducts] = useState([]);
   
  useEffect(()=>{
    fetch('https://dummyjson.com/products')
    .then(res=>res.json())
    .then(data=>setProducts(data.products))
  },[])

    
  return (
    <div className="container">
        <div className='row'>
        {products.map((product)=>(
           <div className="card  m-2 p-1" style={{width: '18rem'}} >
           <img src={product.thumbnail} className="card-img-top" alt={product.title} style={{height:'12rem'}} />
           <div className="card-body">
             <h5 className="card-title">{product.title}</h5>
             <span>Price: {`₹${product.price}`}</span>
             <p className='d-flex justify-content-between'>
                <span className={product.rating > 3.5 ? "bg-success px-2" :"bg-warning"}>{product.rating}</span>
                <span>{product.stock > 0 ? "Available": "SoldOut"}</span>
                </p>

           </div>
         </div>
        ))}
        </div>
    </div>
  )
}

export default UseEffect