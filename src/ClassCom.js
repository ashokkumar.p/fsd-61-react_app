import React from "react";

class ClassCom extends React.Component {

    render(){
        return(
            <>
         <h2>I am learning {this.props.course} in TalentSprint located at {this.props.loc}. </h2>
         
            </>
        )
    }
    
}
export default ClassCom;