import React, { Component } from 'react'

 class ClassCom2 extends Component {
    constructor(){
        super()
        this.state = {
            name:'Ashok'
        }
    }
    handleChange = ()=>{
        this.setState({
            name:"Kumar"
        })
    }
    handleDoubleChange = ()=>{
        this.setState({
            name:"Tarak"
        })
    }
  render() {
    return (
      <div>
        <h1>{this.state.name}</h1>
        <button onClick={this.handleChange}>Click Me</button>
        <button onDoubleClick={this.handleDoubleChange}>DoubleClick</button>
      </div>
    )
  }
}
export default ClassCom2;


// name:
// Age:
// Salary:
// City:
// Button