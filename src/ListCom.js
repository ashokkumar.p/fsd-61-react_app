import React from 'react'

export default function ListCom() {
    const list = ["Ashok","Pavan","Vamsi","Harsha","Deva"];
  return (
    <div>
        <ul>
            {list.map((item)=>(<li>{item}</li>))}
        </ul>
    </div>
  )
}

