import React from 'react'

export default function FunCom1(props) {
  
  return (
    <div>
        <h1>Functional Component Example-1</h1>
        <h3>My name is {props.name} belongs to {props.location} of age is {props.age}.</h3>
    </div>
  )
}
