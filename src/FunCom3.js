import React, { useState } from 'react'
import dog from './assets/dog.jpeg'

export default function FunCom3() {
    const [name,setName] = useState("Ashok");
    const [age,setAge] = useState(21);
   const handleChange = ()=>{
        setName('Kumar')
        setAge(25)
    }
  return (
    <div>
        <h3>{name} {age}</h3>
        <button onClick={handleChange}>Click-1</button>
        <div>
        </div>
        <img src='https://t4.ftcdn.net/jpg/02/66/31/75/360_F_266317554_kr7DPOoM5Uty0YCeFU9nDZTt4a2LeMJF.jpg' alt='Funny'  />
        <img src={dog} alt='' />
    </div>
  )
}
